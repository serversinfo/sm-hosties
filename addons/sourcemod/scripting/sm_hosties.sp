/*
 * SourceMod Hosties Project
 * by: SourceMod Hosties Dev Team
 *
 * This file is part of the SM Hosties project.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <adminmenu>
#include <sdkhooks>
#include <hosties>
#include <emitsoundany>
#include <sm_jail_redie>

#undef REQUIRE_PLUGIN
#undef REQUIRE_EXTENSIONS
#tryinclude <SteamWorks>
#tryinclude <sourcebans>
#define REQUIRE_EXTENSIONS
#define REQUIRE_PLUGIN

// Compiler directives
#pragma 	semicolon 					1

// Constants
#define 	PLUGIN_VERSION				"2.2.13"
#define 	MAX_DISPLAYNAME_SIZE		64
#define 	MAX_DATAENTRY_SIZE			5
#define 	SERVERTAG					"SM Hosties v2.2"

// Note: you cannot safely turn these modules on and off yet. Use cvars to disable functionality.

// Add gun safety
#define	MODULE_GUNSAFETY					1

/******************************************************************************
                   !EDIT BELOW THIS COMMENT AT YOUR OWN PERIL!
******************************************************************************/

// Global vars
bool g_bSBAvailable = false; // SourceBans

Handle gH_TopMenu = null;
TopMenuObject gM_Hosties = INVALID_TOPMENUOBJECT;

#include "hosties/lastrequest.sp"

#if (MODULE_GUNSAFETY)
#include "hosties/gunsafety.sp"
#endif


public Plugin myinfo =
{
	name		= "SM_Hosties v2",
	author		= "databomb & dataviruset & comando",
	description	= "Hosties/jailbreak plugin for SourceMod",
	version		= PLUGIN_VERSION,
	url			= "http://forums.alliedmods.net/showthread.php?t=108810"
};

public void OnPluginStart()
{
	// Load translations
	LoadTranslations("common.phrases");
	LoadTranslations("hosties.phrases");

	// Events hooks
	//HookEvent("round_prestart", Event_RoundPreStart);
	//HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_spawn", EventPlayerSpawn);

	// Create ConVars
	
	CreateConVar("sm_hosties_version", PLUGIN_VERSION, "SM_Hosties plugin version (unchangeable)", FCVAR_NONE|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	
	RegAdminCmd("sm_hostiesadmin", Command_HostiesAdmin, ADMFLAG_SLAY);
	
	
	LastRequest_OnPluginStart();
	
	#if (MODULE_GUNSAFETY)
		GunSafety_OnPluginStart();
	#endif
	AutoExecConfig(true, "sm_hosties2");
}

public void OnMapStart()
{
	LastRequest_OnMapStart();
}

//public OnMapEnd()
//{
//}

public void OnAllPluginsLoaded()
{
	if (LibraryExists("sourcebans"))
		g_bSBAvailable = true;
	Handle h_TopMenu = GetAdminTopMenu();
	if (LibraryExists("adminmenu") && (h_TopMenu != null))
		OnAdminMenuReady(h_TopMenu);
}

public APLRes AskPluginLoad2(Handle h_Myself, bool bLateLoaded, char[] sError, int error_max)
{
	MarkNativeAsOptional("SteamWorks_SetGameDescription");

	LastRequest_APL();
	
	RegPluginLibrary("hosties");
	
	return APLRes_Success;
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "sourcebans"))
		g_bSBAvailable = true;
	else if (StrEqual(name, "adminmenu") && (GetAdminTopMenu() != null))
		OnAdminMenuReady(GetAdminTopMenu());
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "sourcebans"))
		g_bSBAvailable = false;
	else if (StrEqual(name, "adminmenu"))
		gH_TopMenu = GetAdminTopMenu();
}

public void OnConfigsExecuted()
{
	LastRequest_OnConfigsExecuted();
}

public void OnClientPutInServer(int client)
{
	LastRequest_ClientPutInServer(client);
}

//public void Event_RoundPreStart(Handle event, const char[] name, bool dontBroadcast)
//{
//	for(int i=1; i<MAXPLAYERS; i++)
//		if (IsClientConnected(i) && IsClientInGame(i) && IsPlayerAlive(i))
//			g_bDead[i] = false;			// для startweapons
//}
//
//public void Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
//{
//	int client = GetClientOfUserId(GetEventInt(event, "userid"));
//	if (IsClientConnected(client) && IsClientInGame(client)) {
//		g_bDead[client] = true;					// для startweapons
//		PrintToChatAll("%N 444", client);
//	}
//}

public void OnAdminMenuReady(Handle h_TopMenu)
{
	// block double calls
	if (h_TopMenu == gH_TopMenu)
		return;
	
	gH_TopMenu = h_TopMenu;
	
	// Build Hosties menu
	gM_Hosties = AddToTopMenu(gH_TopMenu, "Hosties", TopMenuObject_Category, HostiesCategoryHandler, INVALID_TOPMENUOBJECT);
	
	if (gM_Hosties == INVALID_TOPMENUOBJECT)
		return;
	
	// Let other modules add menu objects
	
	LastRequest_Menus(gH_TopMenu, gM_Hosties);
	
	#if (MODULE_GUNSAFETY)
		GunSafety_Menus(gH_TopMenu, gM_Hosties);
	#endif
}

public Action Command_HostiesAdmin(int client, int args)
{
	DisplayTopMenu(gH_TopMenu, client, TopMenuPosition_LastRoot);
	return Plugin_Handled;
}

public void HostiesCategoryHandler(Handle h_TopMenu, TopMenuAction action, TopMenuObject item, int param, char[] buffer, int maxlength)
{
	switch (action) {
		case (TopMenuAction_DisplayTitle):
			if (item == gM_Hosties)
				Format(buffer, maxlength, "Hosties:");
		case (TopMenuAction_DisplayOption):
			if (item == gM_Hosties)
				Format(buffer, maxlength, "Hosties");
	}
}

public void EventPlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (GetClientTeam(client) == 2) {
		StripAllWeapons(client);
		GivePlayerItem(client, "weapon_knife_t");
	} else if (GetClientTeam(client) == 3) {
		StripAllWeapons(client);
		GivePlayerItem(client, "weapon_knife");
		GivePlayerItem(client, "weapon_usp_silencer");
		GivePlayerItem(client, "weapon_m4a1");
	}
}